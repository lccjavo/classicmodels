<?php

namespace UABC\ClassicModelsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use UABC\ClassicModelsBundle\Entity\Productlines;
use UABC\ClassicModelsBundle\Form\ProductlinesType;

/**
 * Productlines controller.
 *
 */
class ProductlinesController extends Controller
{

    /**
     * Lists all Productlines entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UABCClassicModelsBundle:Productlines')->findAll();

        return $this->render('UABCClassicModelsBundle:Productlines:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Productlines entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Productlines();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('productlines_show', array('id' => $entity->getId())));
        }

        return $this->render('UABCClassicModelsBundle:Productlines:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Productlines entity.
     *
     * @param Productlines $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Productlines $entity)
    {
        $form = $this->createForm(new ProductlinesType(), $entity, array(
            'action' => $this->generateUrl('productlines_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Productlines entity.
     *
     */
    public function newAction()
    {
        $entity = new Productlines();
        $form   = $this->createCreateForm($entity);

        return $this->render('UABCClassicModelsBundle:Productlines:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Productlines entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UABCClassicModelsBundle:Productlines')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Productlines entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('UABCClassicModelsBundle:Productlines:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Productlines entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UABCClassicModelsBundle:Productlines')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Productlines entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('UABCClassicModelsBundle:Productlines:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Productlines entity.
    *
    * @param Productlines $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Productlines $entity)
    {
        $form = $this->createForm(new ProductlinesType(), $entity, array(
            'action' => $this->generateUrl('productlines_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Productlines entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UABCClassicModelsBundle:Productlines')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Productlines entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('productlines_edit', array('id' => $id)));
        }

        return $this->render('UABCClassicModelsBundle:Productlines:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Productlines entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('UABCClassicModelsBundle:Productlines')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Productlines entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('productlines'));
    }

    /**
     * Creates a form to delete a Productlines entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('productlines_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
