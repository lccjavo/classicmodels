<?php

namespace UABC\ClassicModelsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('UABCClassicModelsBundle:Default:index.html.twig', array('name' => $name));
    }
}
