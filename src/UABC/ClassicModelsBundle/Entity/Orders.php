<?php

namespace UABC\ClassicModelsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Orders
 */
class Orders
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $orderdate;

    /**
     * @var \DateTime
     */
    private $requireddate;

    /**
     * @var \DateTime
     */
    private $shippeddate;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $comments;

    /**
     * @var \UABC\ClassicModelsBundle\Entity\Customers
     */
    private $customernumber;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $productcode;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productcode = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderdate
     *
     * @param \DateTime $orderdate
     * @return Orders
     */
    public function setOrderdate($orderdate)
    {
        $this->orderdate = $orderdate;

        return $this;
    }

    /**
     * Get orderdate
     *
     * @return \DateTime 
     */
    public function getOrderdate()
    {
        return $this->orderdate;
    }

    /**
     * Set requireddate
     *
     * @param \DateTime $requireddate
     * @return Orders
     */
    public function setRequireddate($requireddate)
    {
        $this->requireddate = $requireddate;

        return $this;
    }

    /**
     * Get requireddate
     *
     * @return \DateTime 
     */
    public function getRequireddate()
    {
        return $this->requireddate;
    }

    /**
     * Set shippeddate
     *
     * @param \DateTime $shippeddate
     * @return Orders
     */
    public function setShippeddate($shippeddate)
    {
        $this->shippeddate = $shippeddate;

        return $this;
    }

    /**
     * Get shippeddate
     *
     * @return \DateTime 
     */
    public function getShippeddate()
    {
        return $this->shippeddate;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Orders
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return Orders
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set customernumber
     *
     * @param \UABC\ClassicModelsBundle\Entity\Customers $customernumber
     * @return Orders
     */
    public function setCustomernumber(\UABC\ClassicModelsBundle\Entity\Customers $customernumber = null)
    {
        $this->customernumber = $customernumber;

        return $this;
    }

    /**
     * Get customernumber
     *
     * @return \UABC\ClassicModelsBundle\Entity\Customers 
     */
    public function getCustomernumber()
    {
        return $this->customernumber;
    }

    /**
     * Add productcode
     *
     * @param \UABC\ClassicModelsBundle\Entity\Products $productcode
     * @return Orders
     */
    public function addProductcode(\UABC\ClassicModelsBundle\Entity\Products $productcode)
    {
        $this->productcode[] = $productcode;

        return $this;
    }

    /**
     * Remove productcode
     *
     * @param \UABC\ClassicModelsBundle\Entity\Products $productcode
     */
    public function removeProductcode(\UABC\ClassicModelsBundle\Entity\Products $productcode)
    {
        $this->productcode->removeElement($productcode);
    }

    /**
     * Get productcode
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductcode()
    {
        return $this->productcode;
    }
	
	public function __toString(){
		return $this->id." ";
	}
}
