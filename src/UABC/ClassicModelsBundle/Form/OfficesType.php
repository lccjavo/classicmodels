<?php

namespace UABC\ClassicModelsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OfficesType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('city')
            ->add('phone')
            ->add('addressline1')
            ->add('addressline2')
            ->add('state')
            ->add('country')
            ->add('postalcode')
            ->add('territory')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UABC\ClassicModelsBundle\Entity\Offices'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'uabc_classicmodelsbundle_offices';
    }
}
