<?php

namespace UABC\ClassicModelsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductsType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('productname')
            ->add('productscale')
            ->add('productvendor')
            ->add('productdescription')
            ->add('quantityinstock')
            ->add('buyprice')
            ->add('msrp')
            ->add('productline')
            ->add('ordernumber')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UABC\ClassicModelsBundle\Entity\Products'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'uabc_classicmodelsbundle_products';
    }
}
