<?php

namespace UABC\ClassicModelsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EmployeesType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname')
            ->add('firstname')
            ->add('extension')
            ->add('email')
            ->add('jobtitle')
            ->add('officecode')
            ->add('reportsto')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UABC\ClassicModelsBundle\Entity\Employees'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'uabc_classicmodelsbundle_employees';
    }
}
